Source: pnfft
Section: libs
Priority: optional
Maintainer: Ghislain Antony Vaillant <ghisvail@gmail.com>
Build-Depends:
 debhelper (>= 9),
 dh-autoreconf,
 libfftw3-dev,
 libfftw3-mpi-dev,
 libopenmpi-dev,
 libpfft-dev
Standards-Version: 3.9.5
Homepage: https://www-user.tu-chemnitz.de/~mpip/software.php.en#pnfft
Vcs-Git: git://anonscm.debian.org/debian-science/packages/pnfft.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=debian-science/packages/pnfft.git

Package: libpnfft-double0
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Parallel NFFT software library based on MPI -- double precision
 PNFFT is a parallel software library for the calculation of three-dimensional 
 nonequispaced FFTs. It is available under GPL licence. The parallelization 
 is based on MPI.
 .
 This package contains the shared library for double precision transforms.

Package: libpnfft-single0
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Parallel NFFT software library based on MPI -- single precision
 PNFFT is a parallel software library for the calculation of three-dimensional 
 nonequispaced FFTs. It is available under GPL licence. The parallelization 
 is based on MPI.
 .
 This package contains the shared library for single precision transforms.

Package: libpnfft-long0
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Parallel NFFT software library based on MPI -- long double precision
 PNFFT is a parallel software library for the calculation of three-dimensional 
 nonequispaced FFTs. It is available under GPL licence. The parallelization 
 is based on MPI.
 .
 This package contains the shared library for long double precision transforms.

Package: libpnfft-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libpnfft-double0 (= ${binary:Version}),
 libpnfft-single0 (= ${binary:Version}),
 libpnfft-long0 (= ${binary:Version}),
 libpfft-dev
Suggests:
 libpnfft-dbg
Description: Parallel NFFT software library based on MPI -- development
 PNFFT is a parallel software library for the calculation of three-dimensional 
 nonequispaced FFTs. It is available under GPL licence. The parallelization 
 is based on MPI.
 .
 This package contains the header files and static libraries.

Package: libpnfft-dbg
Section: debug
Priority: extra
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 libpnfft-double0 (= ${binary:Version}),
 libpnfft-single0 (= ${binary:Version}),
 libpnfft-long0 (= ${binary:Version})
Suggests:
 libpfft-dbg
Description: Parallel NFFT software library based on MPI -- debug symbols
 PNFFT is a parallel software library for the calculation of three-dimensional 
 nonequispaced FFTs. It is available under GPL licence. The parallelization 
 is based on MPI.
 .
 This package contains the debugging symbols of the libraries.
